#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    if(n >= 0){
        z.x=pow(z.x,2)-pow(z.y,2);
        z.x+=point.x;
        z.y=2*z.x*z.y;
        z.y+=point.y;
        if(sqrt(pow(z.x,2)+pow(z.y,2))>2){
            return 1;
        }else{
            n=n-1;
            return isMandelbrot(z,n,point);
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}
