#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
    int change=1;
    while(change>0){
        change=0;
        for (int i=0; i<toSort.size(); i++) {
            if(i+1==toSort.size()){
                break;
            }
            if(toSort[i]>toSort[i+1]){
                toSort.swap(i, i+1);
                change++;
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
